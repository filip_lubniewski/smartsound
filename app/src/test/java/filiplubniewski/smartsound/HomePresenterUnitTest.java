package filiplubniewski.smartsound;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import filiplubniewski.smartsound.home.HomeContract;
import filiplubniewski.smartsound.home.HomePresenter;
import filiplubniewski.smartsound.songs.Song;
import filiplubniewski.smartsound.songs.SongsRepository;

/**
 * Created by macbookpro on 01.04.2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class HomePresenterUnitTest
{
    @Mock
    SongsRepository songsRepository;
    @Mock
    HomeContract.View view;
    HomePresenter presenter;

    @Before
    public void setUp()
    {
        presenter = new HomePresenter(view, songsRepository);
    }

    @Test
    public void shouldPassSongsToView()
    {
        //given
        List<Song> MANY_SONGS = Arrays.asList(new Song(), new Song(), new Song(), new Song());
        Mockito.when(songsRepository.getSongs()).thenReturn(MANY_SONGS);

        //when
        presenter.loadSongs();

        //then
        Mockito.verify(view).displaySongs(MANY_SONGS);
    }

    @Test public void shouldHandleNoSongsFound()
    {
        //given
        Mockito.when(songsRepository.getSongs()).thenReturn(Collections.EMPTY_LIST);

        //when
        presenter.loadSongs();

        //then
        Mockito.verify(view).displayNoSongs();
    }
}
