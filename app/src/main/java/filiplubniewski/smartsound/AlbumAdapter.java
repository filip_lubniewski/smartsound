package filiplubniewski.smartsound;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import filiplubniewski.smartsound.songs.Song;

/**
 * Created by macbookpro on 12.03.2017.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder>
{
    private List<Song> mSongsList;
    private Context mContext;

    public AlbumAdapter(Context context, List<Song> songsList)
    {
        mSongsList = songsList;
        mContext = context;
    }

    private Context getContext()
    {
        return mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View albumView = inflater.inflate(R.layout.album_tile, parent, false);

        ViewHolder viewHolder = new ViewHolder(albumView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Song song = mSongsList.get(position);

        holder.albumTitle.setText(song.getTitle());
        holder.albumType.setText(song.getArtist());
    }

    @Override
    public int getItemCount()
    {
        return mSongsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView albumTitle;
        public TextView albumType;

        public ViewHolder(View itemView)
        {
            super(itemView);

            albumTitle = (TextView) itemView.findViewById(R.id.album_title);
            albumType = (TextView) itemView.findViewById(R.id.album_type);
        }
    }
}
