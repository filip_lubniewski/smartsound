package filiplubniewski.smartsound.home;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import filiplubniewski.smartsound.R;
import filiplubniewski.smartsound.base.BaseFragment;
import filiplubniewski.smartsound.songs.Song;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by macbookpro on 10.03.2017.
 */

public class HomeFragment extends BaseFragment implements HomeContract.View
{
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final String TAG = HomeFragment.class.getSimpleName();

    private HomeContract.Presenter mPresenter;
    private Context mContext;
    private View mView;

    public static HomeFragment createInstance()
    {
        return new HomeFragment();
    }

    public HomeFragment()
    {

    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mView = inflater.inflate(R.layout.home_fragment, container, false);
        bindButterKnife(mView);

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.start();
    }

    @Override
    public void setPresenter(HomeContract.Presenter presenter)
    {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void displaySongs(List<Song> list)
    {

    }

    @Override
    public void displayNoSongs()
    {

    }

    @Override
    public void showMessageAboutGrantingReadStoragePermission()
    {
        Snackbar snackbar = Snackbar
                .make(mView, getString(R.string.grant_permission_message), Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mPresenter.findSongsToPlay();
                    }
                });

        snackbar.show();
    }

    @Override
    public void askAboutPermissionToReadStorage()
    {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (requestCode == PERMISSION_REQUEST_CODE)
        {
            mPresenter.passRequestPermissionsResult(grantResults);
        }
    }

    @Override
    public void showDialogWithExplanationWhyWeAskAboutReadStoragePermission()
    {
        new AlertDialog.Builder(mContext)
                .setTitle(R.string.read_storage_permission_title)
                .setMessage(mContext.getString(R.string.explanation_of_permission_need))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        askAboutPermissionToReadStorage();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        showMessageAboutGrantingReadStoragePermission();
                    }
                })
                .show();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public Context getContext()
    {
        return mContext;
    }

    @Override
    public Activity getHostActivity()
    {
        return getActivity();
    }
}
