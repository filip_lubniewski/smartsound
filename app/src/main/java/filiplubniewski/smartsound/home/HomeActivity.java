package filiplubniewski.smartsound.home;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import filiplubniewski.smartsound.R;
import filiplubniewski.smartsound.base.BaseActivity;
import filiplubniewski.smartsound.songs.SongsRepository;
import filiplubniewski.smartsound.songs.SongsRetriever;
import filiplubniewski.smartsound.utils.ActivityUtils;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private HomePresenter mHomePresenter;
    private SongsRepository mSongsRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbarSetup();
        drawerSetup();
        contentSetup();
    }

    private void toolbarSetup()
    {
        setSupportActionBar(toolbar);
    }

    private void drawerSetup()
    {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void contentSetup()
    {
        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (homeFragment == null)
        {
            homeFragment = HomeFragment.createInstance();
            ActivityUtils.addFragmentToActivty(getSupportFragmentManager(), homeFragment, R.id.content_frame);
        }

        mHomePresenter = new HomePresenter(homeFragment, SongsRetriever.getInstance(getContentResolver()));
    }

    //TODO save instance

    @Override
    public void onBackPressed()
    {
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera)
        {
            // Handle the camera action
        }
        else if (id == R.id.nav_gallery)
        {

        }
        else if (id == R.id.nav_slideshow)
        {

        }
        else if (id == R.id.nav_manage)
        {

        }
        else if (id == R.id.nav_share)
        {

        }
        else if (id == R.id.nav_send)
        {

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
