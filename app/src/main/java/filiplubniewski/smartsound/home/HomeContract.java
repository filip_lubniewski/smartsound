package filiplubniewski.smartsound.home;

import android.app.Activity;
import android.content.Context;

import java.util.List;

import filiplubniewski.smartsound.base.BasePresenter;
import filiplubniewski.smartsound.base.BaseView;
import filiplubniewski.smartsound.songs.Song;

/**
 * Created by macbookpro on 10.03.2017.
 */

public interface HomeContract
{
    interface View extends BaseView<Presenter>
    {
        void displaySongs(List<Song> list);

        void displayNoSongs();

        void showMessageAboutGrantingReadStoragePermission();

        void askAboutPermissionToReadStorage();

        void showDialogWithExplanationWhyWeAskAboutReadStoragePermission();

        Context getContext();

        Activity getHostActivity();
    }

    interface Presenter extends BasePresenter
    {
        void loadSongs();

        void findSongsToPlay();

        void passRequestPermissionsResult(int[] grantResults);
    }
}
