package filiplubniewski.smartsound.home;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.List;

import filiplubniewski.smartsound.songs.Song;
import filiplubniewski.smartsound.songs.SongsRepository;
import filiplubniewski.smartsound.utils.PermissionUtils;

/**
 * Created by macbookpro on 10.03.2017.
 */

public class HomePresenter implements HomeContract.Presenter
{
    HomeContract.View mView;
    private SongsRepository mSongsRepository;
    Context mContext;

    public HomePresenter(HomeContract.View view, SongsRepository songsRepository)
    {
        mView = view;
        mView.setPresenter(this);

        mSongsRepository = songsRepository;
    }

    @Override
    public void start()
    {
        mContext = mView.getContext();

        findSongsToPlay();
    }

    public void findSongsToPlay()
    {
        if (PermissionUtils.isNecessaryToAskUserAboutPermission())
        {
            checkPermissions();
        }
        else
        {
            loadSongs();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissions()
    {
        if (PermissionUtils.isPermissionToReadStorageGranted(mContext))
        {
            loadSongs();
        }
        else
        {
            if (mView.getHostActivity().shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                mView.showDialogWithExplanationWhyWeAskAboutReadStoragePermission();
            }
            else
            {
                mView.askAboutPermissionToReadStorage();
            }
        }
    }

    @Override
    public void loadSongs()
    {
        List<Song> list = mSongsRepository.getSongs();

        mView.displaySongs(list);
    }

    @Override
    public void passRequestPermissionsResult(int[] grantResults)
    {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            loadSongs();
        }
        else
        {
            mView.showMessageAboutGrantingReadStoragePermission();
        }
    }
}


