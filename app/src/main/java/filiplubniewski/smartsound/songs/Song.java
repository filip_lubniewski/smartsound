package filiplubniewski.smartsound.songs;

/**
 * Created by macbookpro on 17.03.2017.
 */

public class Song
{
    private String mTitle;
    private String mArtist;
    private long mDuration;
    private long mId;

    public Song()
    {
    }

    public static Song createSong()
    {
        return new Song();
    }

    public Song setTitle(String title)
    {
        mTitle = title;
        return this;
    }

    public Song setArtist(String artist)
    {
        mArtist = artist;
        return this;
    }

    public Song setDuration(long duration)
    {
        mDuration = duration;
        return this;
    }

    public Song setId(long id)
    {
        mId = id;
        return this;
    }

    public String getTitle()
    {
        return mTitle;
    }

    public String getArtist()
    {
        return mArtist;
    }

    public long getId()
    {
        return mId;
    }
}
