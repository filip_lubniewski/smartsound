package filiplubniewski.smartsound.songs;

import java.util.List;

/**
 * Created by macbookpro on 01.04.2017.
 */

public interface SongsRepository
{
    List<Song> getSongs();
}
