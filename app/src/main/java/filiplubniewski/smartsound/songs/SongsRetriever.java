package filiplubniewski.smartsound.songs;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 17.03.2017.
 */

public class SongsRetriever implements SongsRepository
{
    private static final String TAG = SongsRetriever.class.getSimpleName();
    private static SongsRetriever mInstance;

    private ContentResolver mContentResolver;
    private List<Song> mSongsList = new ArrayList<>();

    private SongsRetriever(ContentResolver contentResolver)
    {
        mContentResolver = contentResolver;
    }

    public static SongsRetriever getInstance(ContentResolver contentResolver)
    {
        if (mInstance == null)
        {
            mInstance = new SongsRetriever(contentResolver);
        }

        return mInstance;
    }

    @Override
    public List<Song> getSongs()
    {
        makeQuery();
        return mSongsList;
    }

    private void makeQuery()
    {
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        Log.d(TAG, "Querying songs...");
        Log.d(TAG, "music URI: " + musicUri.toString());

        Cursor cursor = getContentResolver().query(musicUri, null, null, null, null);

        if (cursor == null)
        {
            Log.e(TAG, "Query failed, cursor is NULL");
            return;
        }

        if (!cursor.moveToFirst())
        {
            Log.e(TAG, "No songs on this device!");
            return;
        }

        Log.d(TAG, "Looking for songs!");

        int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int durationColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
        int idColumn = cursor.getColumnIndex(MediaStore.Audio.Media._ID);

        do
        {
            Log.d(TAG, "SONG ID: " + cursor.getString(idColumn) + " TITLE: " + cursor.getString(titleColumn));

            mSongsList.add(Song
                    .createSong()
                    .setTitle(cursor.getString(titleColumn))
                    .setArtist(cursor.getString(artistColumn))
                    .setDuration(cursor.getLong(durationColumn))
                    .setId(cursor.getLong(idColumn)));

        } while (cursor.moveToNext());

    }

    private ContentResolver getContentResolver()
    {
        return mContentResolver;
    }
}
