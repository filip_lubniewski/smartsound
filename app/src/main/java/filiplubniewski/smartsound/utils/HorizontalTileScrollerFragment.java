package filiplubniewski.smartsound.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import filiplubniewski.smartsound.AlbumAdapter;
import filiplubniewski.smartsound.R;
import filiplubniewski.smartsound.base.BaseFragment;
import filiplubniewski.smartsound.songs.Song;

/**
 * Created by macbookpro on 12.03.2017.
 */

public class HorizontalTileScrollerFragment extends BaseFragment
{
    @BindView(R.id.album_horizontal_list)
    RecyclerView albumHorizontalList;

    private Context mContext;

    public HorizontalTileScrollerFragment()
    {
    }

    public HorizontalTileScrollerFragment getInstance(List<Song> songsList)
    {

    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.horizontal_tile_scroller, container, false);
        bindButterKnife(view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        albumHorizontalList.setLayoutManager(layoutManager);
        List<String> ls = new ArrayList<>();
        ls.add("rihanna");
        ls.add("rihanna");
        ls.add("rihanna");
        ls.add("rihanna");
        ls.add("rihanna");
        ls.add("rihanna");
        ls.add("rihanna");

        AlbumAdapter adapter = new AlbumAdapter(mContext, ls);
        albumHorizontalList.setAdapter(adapter );

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

    }
}
