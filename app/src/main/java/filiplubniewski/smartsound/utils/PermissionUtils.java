package filiplubniewski.smartsound.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by macbookpro on 01.04.2017.
 */

public class PermissionUtils
{
    public static boolean isNecessaryToAskUserAboutPermission()
    {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean isPermissionToReadStorageGranted(Context context)
    {
        return context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }
}
