package filiplubniewski.smartsound.base;

import android.support.v4.app.Fragment;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by macbookpro on 11.03.2017.
 */

public class BaseFragment extends Fragment
{
    private Unbinder mUnbinder;

    protected void bindButterKnife(View view)
    {
        mUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mUnbinder.unbind();
    }
}
