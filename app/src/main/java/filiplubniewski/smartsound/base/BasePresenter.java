package filiplubniewski.smartsound.base;

/**
 * Created by macbookpro on 10.03.2017.
 */

public interface BasePresenter
{
    void start();
}
