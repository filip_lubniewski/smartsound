package filiplubniewski.smartsound.base;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by macbookpro on 11.03.2017.
 */

public class BaseActivity extends AppCompatActivity
{
    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        super.setContentView(layoutResID);
        ButterKnife.bind(this,this);
    }
}
